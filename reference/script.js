$( document ).ready(function() {

  var slider = document.getElementById("myRange");
  var output = document.getElementById("demo");
  output.innerHTML = slider.value; // Display the default slider value

  // Update the current slider value (each time you drag the slider handle)
  slider.oninput = function() {
    output.innerHTML = this.value;
  }



  $("#btnCalc").on("click", () => {
    var today = getTodayFormatedDate();
    var date = getFormatedDate($("#date1").val());
    var resultField = $("#result");

    var res = date_diff_indays(today, date)
    res = Number(res) <= 5 ?
                        Number(res) < 0 ? "Outdate" + res :
                        Number(res) == 0 ? "Is Today - " + res :
                        "Date is close - " + res
                        : "Normal - " + res;

    $(resultField).text(res);
  })

});

function date_diff_indays (date1, date2) {
  dt1 = new Date(date1);
  dt2 = new Date(date2);
  return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate()) ) /(1000 * 60 * 60 * 24));
}

function getTodayFormatedDate(){
  let formatedDate = "01-01-1979";

  let now = new Date();
  let  day = ("0" + now.getDate()).slice(-2);
  let  month = ("0" + (now.getMonth() + 1)).slice(-2);
  formatedDate = now.getFullYear() + "-" + (month) + "-" + (day);

  return formatedDate;
}

function getFormatedDate(date){
  let formatedDate = "01-01-1979";

  let now = new Date(date);
  let  day = ("0" + now.getDate()).slice(-2);
  let  month = ("0" + (now.getMonth() + 1)).slice(-2);
  formatedDate = now.getFullYear() + "-" + (month) + "-" + (day);

  return formatedDate;
}
