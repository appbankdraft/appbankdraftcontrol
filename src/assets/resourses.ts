export const MonthsByName = {
  "January" : "Janeiro",
  "February" : "Fevereiro",
  "March" : "Março",
  "April" : "Abril",
  "May" : "Maio",
  "June" : "Junho",
  "July" : "Julho",
  "August" : "Agosto",
  "September" : "Setembro",
  "October" : "Outubro",
  "November" : "Novembro",
  "December" : "Dezembro",
}

export const MonthsByNumber = {
  1 : "Janeiro",
  2 : "Fevereiro",
  3 : "Março",
  4 : "Abril",
  5 : "Maio",
  6 : "Junho",
  7 : "Julho",
  8 : "Agosto",
  9 : "Setembro",
  10 : "Outubro",
  11 : "Novembro",
  12 : "Dezembro",
}

export const FieldsName = {
  "DraftNumber":"Numero do cheque",
  "Notes":"Observações",
  "PaymentDate":"Data de pagamento",
  "Photo":"Foto do cheque",
  "StoredUpValue":"Valor já obtido",
  "Supplier":"Fornecedor",
  "Value":"Valor",
  "isCanceled":"Cancelado?",
  "isDateClose":"Perto da data de pagamento?",
  "isOutdatedAndNotPaid":"Ultrapassou o prazo e não foi pago?",
  "isPaid":"Pago?",
}

export const FormatAsText= [
  "DraftNumber",
  "Supplier",
]

export const FormatAsCurrency = [
  "StoredUpValue",
  "Value"
]

export const FormatAsDate = [
  "PaymentDate",
]

export const FormatAsRadioButton = [
  "isCanceled",
  "isDateClose",
  "isOutdatedAndNotPaid",
  "isPaid"
]

export const FormatAsImage = [
  "Photo"
]

export const FormatAsTextArea = [
  "Notes"
]

export const DRAFTS_API = "http://localhost:5000/api";
