import { Component, OnInit } from '@angular/core';
import { DraftListItem } from '../models/draft-item.model';
import { ActivatedRoute, Router } from '@angular/router';
import { DraftItemListService } from '../services/draft.list.item.service';
import { MonthsByName } from 'src/assets/resourses';
import { getTodayFormatedDate, getFormatedDate } from '../Helpers/utils';

@Component({
  selector: 'abd-update-draft',
  templateUrl: './update-draft.component.html',
  styleUrls: ['./update-draft.component.css']
})
export class UpdateDraftComponent implements OnInit {

  draft: DraftListItem
  isUpdated: boolean
  monthNames: any
  id: any
  draftPhoto: string
  rows: number
  today: string

  constructor(private route: ActivatedRoute, private router: Router, private restApi: DraftItemListService) {
    this.isUpdated = false
    this.monthNames = MonthsByName;

    this.today = getTodayFormatedDate();
  }

  ngOnInit() {
    this.draft = window.history.state
    this.id = this.draft.id === undefined ? this.route.snapshot.params.id : this.draft.id

    if(this.draft.id === undefined){
      this.getDraft(this.id);
    }
  }

  getDraft(id){
    this.restApi.getDraft(id).subscribe(( data: any) => {
      let tmpDate = getFormatedDate(data.paymentDate);

      data.paymentDate = tmpDate;
      this.draft = data;

      if(this.draft.photo && this.draft.photo !== undefined){
        this.draftPhoto = `../../../assets/drafts/${this.draft.photo}`
      }else{
        this.draftPhoto = "../../../assets/example.jpg"
      }
      if(this.draft.notes !== null){
        let notesLenght = this.draft.notes.length
        this.rows = Number( (Number(notesLenght)/31).toFixed() ) < 2 ? 3 : Number( (Number(notesLenght)/31).toFixed() )
      }else{
        this.rows = 3;
      }

    })
  }

  onSubmit() {
    this.restApi.updateDraft(this.draft).subscribe(( data: any) => {
      this.isUpdated = true
    });
  }

}
