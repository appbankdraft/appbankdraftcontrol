import { Component, OnInit, Input } from '@angular/core';
import { DraftDetailItem } from 'src/app/models/draft-detail-item.model';
import { FieldsName, MonthsByName, FormatAsText, FormatAsDate, FormatAsCurrency, FormatAsImage, FormatAsRadioButton, FormatAsTextArea } from 'src/assets/resourses';

@Component({
  selector: 'abd-update-draft-item',
  templateUrl: './update-draft-item.component.html'
})
export class UpdateDraftItemComponent implements OnInit {

  @Input() data: DraftDetailItem
  aux: any
  translator: any
  months: any

  formatAsNormal: any
  formatAsDate: any
  formatAsCurrency: any
  formatAsImage: any
  formatAsTextArea: any
  formatAsRadioBtn: any
  isToUpdate: boolean
  draftPhoto: string
  rows: number

  constructor() {
    this.translator = FieldsName;
    this.months = MonthsByName;

    this.formatAsNormal = FormatAsText;
    this.formatAsDate = FormatAsDate;
    this.formatAsCurrency = FormatAsCurrency;
    this.formatAsImage = FormatAsImage;
    this.formatAsTextArea = FormatAsTextArea;
    this.formatAsRadioBtn = FormatAsRadioButton;
    this.isToUpdate = false;
  }

  ngOnInit() {
    if(this.data.Description === "Notes"){
      let x = this.data.Value.length
      this.rows = Number( (Number(x)/32).toFixed() ) < 2 ? 3 : Number( (Number(x)/32).toFixed() )
    }

    if(this.data.Description === "Photo"){
      if(this.data.Value && this.data.Value !== undefined && this.data.Value !== ""){
        this.draftPhoto = `../../../assets/drafts/${this.data.Value}`
      }else{
        this.draftPhoto = "../../../assets/example.jpg"
      }
    }
  }

  updateDraft(e) {
    // Usage!
    this.sleep(5000).then(() => {
      // Do something after the sleep!
      console.log("DONE");
      this.isToUpdate = true
    })
    e.preventDefault();
  }

  sleep (time) {
    return new Promise((resolve) => setTimeout(resolve, time));
  }

  updateState(e, what){
    switch(what){
      case "isPaid":
        console.log("IsPaid");
        break;
      case "isDateClose":
        console.log("isDateClose");
        break;
      case "isOutdatedAndNotPaid":
        console.log("isOutdatedAndNotPaid");
        break;
      case "isCanceled":
        console.log("isCanceled");
        break;
      default:
        console.log("default")
        break;
    }
  }

}
