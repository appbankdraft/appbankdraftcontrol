import { Injectable } from '@angular/core';
import { DraftListItem } from '../Models/draft-item.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError, map, tap } from 'rxjs/operators';
import { DRAFTS_API } from 'src/assets/resourses';
import { DraftDTO } from './../models/draft.dto';

@Injectable({
  providedIn: 'root'
})

export class DraftItemListService{
    draftlistitem: DraftListItem[]
    baseUrl: string;

    constructor(private http: HttpClient){
        this.baseUrl = DRAFTS_API;
    }

    //Http Options
    httpOptions = {
        headers: new HttpHeaders({
        'Content-Type': 'application/json'
        })
    }

    // HttpClient API get() method => Fetch employees list
    getDrafts(): Observable<DraftListItem> {
        let date = this.getDateNow();
        return this.http.get<DraftListItem>(`${this.baseUrl}/drafts`)
            .pipe(
                retry(1), catchError(this.handleError)
            )
    }

    // HttpClient API get() method => Fetch employees list
    getNotPaidAndFromThisMonthToFowardDrafts(): Observable<DraftListItem> {
        let date = this.getDateNow();
        return this.http.get<DraftListItem>(this.baseUrl + `/drafts/GetInProgressDrafts`)
            .pipe(
                retry(1), catchError(this.handleError)
            )
    }

    // HttpClient API get() method => Fetch employees list
    getGetPaidAndCanceledDrafts(): Observable<DraftListItem> {
        let date = this.getDateNow();
        return this.http.get<DraftListItem>(this.baseUrl + `/drafts/GetPaidAndCanceled`)
            .pipe(
                retry(1), catchError(this.handleError)
            )
    }

    // HttpClient API get() method => Fetch employees list
    getPaidDrafts(): Observable<DraftListItem> {
        let date = this.getDateNow();
        return this.http.get<DraftListItem>(this.baseUrl + `/draft?isPaid=true&_sort=PaymentDate&_order=ASC`)
            .pipe(
                retry(1), catchError(this.handleError)
            )
    }

    // HttpClient API get() method => Fetch employees list
    getCanceledDrafts(): Observable<DraftListItem> {
        let date = this.getDateNow();
        return this.http.get<DraftListItem>(this.baseUrl + `/draft?isPaid=false&isCanceled=true&_sort=PaymentDate&_order=ASC`)
            .pipe(
                retry(1), catchError(this.handleError)
            )
    }

    // HttpClient API get() method => Fetch employee
    getDraft(id): Observable<DraftListItem> {
        return this.http.get<DraftListItem>(this.baseUrl + '/drafts/' + id)
            .pipe( retry(1),catchError(this.handleError)
        )
    }

    // HttpClient API put() method => update the hero on the server */
    //updateHero (hero: Hero): Observable<any> {
    createDraft(draft: DraftDTO): Observable<any> {
      return this.http.post(`${this.baseUrl}/drafts`, draft, this.httpOptions)
      .pipe(
        tap(_ => console.log(`Created draft id: ${draft.draftNumber}`) ),
        catchError(this.handleError)
      );
    }

    // HttpClient API put() method => update the hero on the server */
    //updateHero (hero: Hero): Observable<any> {
    updateDraft(draft: DraftListItem): Observable<any> {
      return this.http.put(`${this.baseUrl}/drafts/${draft.id}`, draft, this.httpOptions)
      .pipe(
        tap(_ => console.log(`Updated draft id: ${draft.id}`) ),
        catchError(this.handleError)
      );
    }

    // HttpClient API put() method => update the hero on the server */
    //updateHero (hero: Hero): Observable<any> {
    deleteDraft(draftId: number): Observable<any> {
      return this.http.delete(`${this.baseUrl}/drafts/${draftId}`, this.httpOptions)
      .pipe(
        tap(_ => console.log(`Draft with id: ${draftId} was deleted`) ),
        catchError(this.handleError)
      );
    }

    // Error handling
    handleError(error) {
        let errorMessage = '';
        if(error.error instanceof ErrorEvent) {
            // Get client-side error
            errorMessage = error.error.message;
        } else {
            // Get server-side error
            errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        }

        window.alert(errorMessage);
        return throwError(errorMessage);
    }

    getDateNow(){
      let today = new Date();
      let dd = "01";
      let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
      let yyyy = today.getFullYear();

      return (yyyy + '-' + mm + '-' + dd);
    }
}
