import {Injectable, EventEmitter} from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class EventEmiterService {

  dataStr = new EventEmitter();

  constructor() { }

  sendMessage(title: string) {
    this.dataStr.emit(title);
  }
}
