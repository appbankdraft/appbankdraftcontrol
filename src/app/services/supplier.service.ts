import { Injectable } from '@angular/core';
import { DraftListItem } from '../Models/draft-item.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError, map, tap } from 'rxjs/operators';
import { DRAFTS_API } from 'src/assets/resourses';

@Injectable({
  providedIn: 'root'
})

export class SuppliersService{
    SupplierList: any[]
    baseUrl: string;

    constructor(private http: HttpClient){
      this.baseUrl = DRAFTS_API;
    }

    //Http Options
    httpOptions = {
        headers: new HttpHeaders({
        'Content-Type': 'application/json'
        })
    }

    // HttpClient API get() method => Fetch employees list
    getSuppliers(): Observable<any> {
        let date = this.getDateNow();
        return this.http.get<any>(this.baseUrl + `/suppliers`)
            .pipe(
                retry(1), catchError(this.handleError)
            )
    }

    // HttpClient API get() method => Fetch employees list
    getSupplierById(id: any): Observable<DraftListItem> {
        let date = this.getDateNow();
        return this.http.get<any>(this.baseUrl + `/suppliers/${id}`)
            .pipe(
                retry(1), catchError(this.handleError)
            )
    }

    // HttpClient API put() method => update the hero on the server */
    //updateHero (hero: Hero): Observable<any> {
    createSupplier(supplier: any): Observable<any> {
      return this.http.post(`${this.baseUrl}/suppliers`, supplier, this.httpOptions)
      .pipe(
        tap(_ => console.log(`Supplier created with id: ${supplier.id}`) ),
        catchError(this.handleError)
      );
    }

    // HttpClient API put() method => update the hero on the server */
    //updateHero (hero: Hero): Observable<any> {
    deleteSupplier(supplierId: number): Observable<any> {
      return this.http.delete(`${this.baseUrl}/suppliers/${supplierId}`, this.httpOptions)
      .pipe(
        tap(_ => console.log(`Supplier with id: ${supplierId} was deleted`) ),
        catchError(this.handleError)
      );
    }

    // Error handling
    handleError(error) {
        let errorMessage = '';
        if(error.error instanceof ErrorEvent) {
            // Get client-side error
            errorMessage = error.error.message;
        } else {
            // Get server-side error
            errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        }

        window.alert(errorMessage);
        return throwError(errorMessage);
    }

    getDateNow(){
      let today = new Date();
      let dd = "01";
      let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
      let yyyy = today.getFullYear();

      return (yyyy + '-' + mm + '-' + dd);
    }
}
