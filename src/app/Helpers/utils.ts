export function getFormatedDate(date: Date){
  let formatedDate = "01-01-1979";

  let now = new Date(date);
  let  day = ("0" + now.getDate()).slice(-2);
  let  month = ("0" + (now.getMonth() + 1)).slice(-2);
  formatedDate = now.getFullYear() + "-" + (month) + "-" + (day);

  return formatedDate;
}

export function getTodayFormatedDate(){
  let formatedDate = "01-01-1979";

  let now = new Date();
  let  day = ("0" + now.getDate()).slice(-2);
  let  month = ("0" + (now.getMonth() + 1)).slice(-2);
  formatedDate = now.getFullYear() + "-" + (month) + "-" + (day);

  return formatedDate;
}

export function getDateDiffInDays(date1: string, date2: string) {
  let dt1 = new Date(date1);
  let dt2 = new Date(date2);

  return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate()) ) /(1000 * 60 * 60 * 24));
}
