import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { DraftListItem } from '../../models/draft-item.model';
import { MonthsByName } from '../../../assets/resourses';
import { DraftItemListService } from './../../services/draft.list.item.service';

@Component({
  selector: 'abd-draft-list-item',
  templateUrl: './draft-list-item.component.html'
})
export class DraftListItemComponent implements OnInit {

  @Output() deleted = new EventEmitter();
  @Input() draftList : DraftListItem
  monthNames: any

  constructor(private restApi: DraftItemListService) {
    this.monthNames = MonthsByName;
  }

  ngOnInit() {

  }

  onDelete(id){
    this.restApi.deleteDraft(id).subscribe(( data: any) => {
      this.deleted.emit();
    });
  }

  onChange(draft, type){
    switch(type){
      case "canceled":
        draft.isCanceled = true;
        break;
      case "paid":
          draft.isPaid = true;
          break;
      default:
        break;
    }

    this.restApi.updateDraft(draft).subscribe(( data: any) => {
      this.deleted.emit();
    });
  }

}
