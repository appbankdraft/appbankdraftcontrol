import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DraftListItem } from '../models/draft-item.model';
import {DraftItemListService} from '../services/draft.list.item.service'
import { MonthsByNumber } from '../../assets/resourses';
import { EventEmiterService } from './../services/event.emiter.service';
import { getTodayFormatedDate, getDateDiffInDays } from '../Helpers/utils';

@Component({
  selector: 'abd-draft-list',
  templateUrl: './draft-list.component.html'
})
export class DraftListComponent implements OnInit {

  draftList : DraftListItem[]
  months: any;
  draftItems: any = []
  today: string

  constructor(private dit: DraftItemListService, private eventEmiter: EventEmiterService) {
    this.today = getTodayFormatedDate()
  }

  ngOnInit() {
    this.months = MonthsByNumber;

    this.getDrafts();
    this.eventEmiter.sendMessage(`Lista de Cheques`);
  }

  getDrafts(){
    return this.dit.getNotPaidAndFromThisMonthToFowardDrafts().subscribe(( data: {}) => {
      this.loadDrafts(data);
    })
  }

  loadDrafts(aux: any){
    let first = true
    let prevMonth = null;
    let prevYear = null;
    let data = []

    aux.forEach( d =>{
        d.isOutdatedAndNotPaid = getDateDiffInDays(this.today, d.paymentDate) < 0;
        d.isDateClose = getDateDiffInDays(this.today, d.paymentDate) >= 0 && getDateDiffInDays(this.today, d.paymentDate) <= 5;

        if(first){
          first = false
          prevMonth = (new Date(d.paymentDate).getMonth() + 1)
          prevYear = new Date(d.paymentDate).getFullYear()

          //data.push(this.months[(d.paymentDate.getMonth() + 1) ])
          //+ " '" + String(new Date(d.paymentDate).getFullYear()).slice(-2)
          data.push({month: this.months[(new Date(d.paymentDate).getMonth() + 1) ], year: String(new Date(d.paymentDate).getFullYear()).slice(-2), total: d.Value})
          data.push(d)

        }else if((new Date(d.paymentDate).getMonth() + 1) === prevMonth && new Date(d.paymentDate).getFullYear() === prevYear ){
          data.push(d)
          data.map( obj => {
            if(this.months[(new Date(d.paymentDate).getMonth() + 1)] == obj.month){
              obj.total = obj.total + d.Value
            }
          })
        }else{
          //data.push(this.months[(d.paymentDate.getMonth() + 1) ])
          //+ " '" + String(new Date(d.paymentDate).getFullYear()).slice(-2)
          data.push({month: this.months[(new Date(d.paymentDate).getMonth() + 1) ], year: String(new Date(d.paymentDate).getFullYear()).slice(-2), total: d.Value})
          data.push(d)

          //totals.push({ month: (d.paymentDate.getMonth()+1), total: d.Value})
        }

        prevMonth = (new Date(d.paymentDate).getMonth() + 1)
        prevYear = new Date(d.paymentDate).getFullYear()

    })
    this.draftList = []
    this.draftList = data

  }
}
