import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { HomeComponent } from './home/home.component';
import { DraftListComponent } from './draft-list/draft-list.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DraftDetailsComponent } from './draft-details/draft-details.component';
import { CreateDraftComponent } from './create-draft/create-draft.component';
import { UpdateDraftComponent } from './update-draft/update-draft.component';
import { DraftListItemComponent } from './draft-list/draft-list-item/draft-list-item.component';
import { HttpClientModule } from '@angular/common/http';
import { DraftItemListService } from './services/draft.list.item.service';
import { CreateNewSupplierComponent } from './create-new-supplier/create-new-supplier.component';
import { DraftDetailsItemComponent } from './draft-details/draft-details-item/draft-details-item.component';
import { UpdateDraftItemComponent } from './update-draft/update-draft-item/update-draft-item.component';
import { CreateDraftItemComponent } from './create-draft/create-draft-item/create-draft-item.component';
import { DraftPaidAndCanceledListComponent } from './draft-paid-and-canceled-list/draft-paid-and-canceled-list.component';
import { DraftPaidAndCanceledListItemComponent } from './draft-paid-and-canceled-list/draft-paid-and-canceled-list-item/draft-paid-and-canceled-list-item.component';
import { SupplierListComponent } from './supplier-list/supplier-list.component';
import { SuppliersService } from './services/supplier.service';
import { EventEmiterService } from './services/event.emiter.service';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    HomeComponent,
    DraftListComponent,
    DraftDetailsComponent,
    CreateDraftComponent,
    UpdateDraftComponent,
    DraftListItemComponent,
    CreateNewSupplierComponent,
    DraftDetailsItemComponent,
    UpdateDraftItemComponent,
    CreateDraftItemComponent,
    DraftPaidAndCanceledListComponent,
    DraftPaidAndCanceledListItemComponent,
    SupplierListComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule
  ],
  providers: [DraftItemListService, SuppliersService, EventEmiterService],
  bootstrap: [AppComponent]
})
export class AppModule { }
