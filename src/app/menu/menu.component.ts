import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { EventEmiterService } from '../services/event.emiter.service';

@Component({
  selector: 'abd-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  pageTitle: string
  constructor(private title: Title, private route: ActivatedRoute, private eventEmiter: EventEmiterService) {

    this.route.data.subscribe( v =>
      this.pageTitle = v.title
    );

    if(this.pageTitle !== undefined && this.pageTitle !== ""){
      this.title.setTitle("Control de Cheques - " + this.pageTitle)
    }else{
      this.pageTitle = "Menu";
      this.title.setTitle("Controlo de Cheques")
    }
  }

  ngOnInit() {
    this.setDataStr();
  }

  myFunction(event) {
    if(event !== undefined && event.target.innerText !== ""){
      this.changeMenuTitle(event.target.innerText);
    }

    var x = document.getElementById("myLinks");

    if (x.style.display === "block") {
      x.style.display = "none";
    } else {
      x.style.display = "block";
    }
  }

  changeMenuTitle(newTitle){
    this.pageTitle = newTitle;
    this.title.setTitle( this.pageTitle )
  }

  setDataStr() {
    this.eventEmiter.dataStr.subscribe(data => this.changeMenuTitle(data))
  }
}
