export class DraftListItem{

  constructor(
    public id: string,
    public draftNumber: string,
    public supplier: string,
    public paymentDate: Date,
    public value: number,
    public storedUpValue: number,
    public photo: Blob,
    public notes: string,
    public isPaid: boolean,
    public isCanceled: boolean,
    public isDateClose: boolean,
    public isOutdatedAndNotPaid: boolean,
    public month: string,
    public total: number
  ){ }
}
