export class DraftDTO{

  constructor(
    public draftNumber: string,
    public supplier: string,
    public paymentDate: Date,
    public value: number,
    public storedUpValue: number,
    public photo: File,
    public notes: string,
    public isPaid: boolean,
    public isCanceled: boolean,
    public isOutdatedAndNotPaid: boolean
  ){ }
}
