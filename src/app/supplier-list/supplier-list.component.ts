import { Component, OnInit } from '@angular/core';
import { SuppliersService } from '../services/supplier.service';

@Component({
  selector: 'abd-supplier-list',
  templateUrl: './supplier-list.component.html'
})
export class SupplierListComponent implements OnInit {

  suppliers: any = []
  constructor(private dit: SuppliersService) {
  }

  ngOnInit() {
    this.getSuppliers();
  }

  getSuppliers(){
    return this.dit.getSuppliers().subscribe(( data: {}) => {
      this.suppliers = data;
    })
  }
}
