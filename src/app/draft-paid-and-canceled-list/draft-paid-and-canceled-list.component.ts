import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DraftListItem } from '../models/draft-item.model';
import {DraftItemListService} from '../services/draft.list.item.service'
import { MonthsByNumber } from '../../assets/resourses';
import { EventEmiterService } from '../services/event.emiter.service';

@Component({
  selector: 'abd-draft-paid-and-canceled-list',
  templateUrl: './draft-paid-and-canceled-list.component.html'
})
export class DraftPaidAndCanceledListComponent implements OnInit {

    draftList : DraftListItem[]
    months: any;
    draftItems: any = []
    action: string
    constructor(private route:ActivatedRoute, private router:Router, private dit: DraftItemListService, private eventEmiter: EventEmiterService) {
      this.action = route.snapshot.data['action'];
    }

    ngOnInit() {
      this.months = MonthsByNumber;

      this.getDrafts();
      this.eventEmiter.sendMessage(`Cheques Pagos/Anulados`);
    }

    getDrafts(){
      return this.dit.getGetPaidAndCanceledDrafts().subscribe(( data: {}) => {
        this.loadDrafts(data);
      })
    }

    loadDrafts(aux: any){
      let first = true
      let prevMonth = null;
      let prevYear = null;
      let data = []

      aux.forEach( d =>{
          if(first){
            first = false
            prevMonth = (new Date(d.paymentDate).getMonth() + 1)
            prevYear = new Date(d.paymentDate).getFullYear()

            //data.push(this.months[(d.PaymentDate.getMonth() + 1) ])
            data.push({month: this.months[(new Date(d.paymentDate).getMonth() + 1) ], total: d.Value})
            data.push(d)

          }else if((new Date(d.paymentDate).getMonth() + 1) === prevMonth && new Date(d.paymentDate).getFullYear() === prevYear ){
            data.push(d)
            data.map( obj => {
              if(this.months[(new Date(d.paymentDate).getMonth() + 1)] == obj.month){
                obj.total = obj.total + d.Value
              }
            })
          }else{
            //data.push(this.months[(d.PaymentDate.getMonth() + 1) ])
            data.push({month: this.months[(new Date(d.paymentDate).getMonth() + 1) ], total: d.Value})
            data.push(d)

            //totals.push({ month: (d.PaymentDate.getMonth()+1), total: d.Value})
          }

          prevMonth = (new Date(d.paymentDate).getMonth() + 1)
          prevYear = new Date(d.paymentDate).getFullYear()
      })

      this.draftList = []
      this.draftList = data

    }
  }

