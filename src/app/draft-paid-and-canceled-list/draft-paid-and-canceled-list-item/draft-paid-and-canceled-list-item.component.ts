import { Component, OnInit, Input } from '@angular/core';
import { DraftListItem } from 'src/app/models/draft-item.model';
import { MonthsByName } from 'src/assets/resourses';

@Component({
  selector: 'abd-draft-paid-and-canceled-list-item',
  templateUrl: './draft-paid-and-canceled-list-item.component.html'
})
export class DraftPaidAndCanceledListItemComponent implements OnInit {

  @Input() draftList : DraftListItem
  monthNames: any

  constructor() {
    this.monthNames = MonthsByName;
  }

  ngOnInit() {

  }

}
