import { Component, OnInit, Input } from '@angular/core';
import { DraftDetailItem } from 'src/app/models/draft-detail-item.model';
import { FieldsName, FormatAsText, FormatAsDate, FormatAsCurrency, FormatAsImage, FormatAsTextArea, FormatAsRadioButton } from 'src/assets/resourses';

@Component({
  selector: 'abd-create-draft-item',
  templateUrl: './create-draft-item.component.html'
})
export class CreateDraftItemComponent implements OnInit {


  @Input() data: DraftDetailItem
  aux: any
  translator: any
  months: any

  formatAsNormal: any
  formatAsDate: any
  formatAsCurrency: any
  formatAsImage: any
  formatAsTextArea: any
  formatAsRadioBtn: any
  isToUpdate: boolean
  isToUpdatedd: boolean


  constructor() {
    this.translator = FieldsName;

    this.formatAsNormal = FormatAsText;
    this.formatAsDate = FormatAsDate;
    this.formatAsCurrency = FormatAsCurrency;
    this.formatAsImage = FormatAsImage;
    this.formatAsTextArea = FormatAsTextArea;
    this.formatAsRadioBtn = FormatAsRadioButton;
  }

  ngOnInit() {

  }

  updateState(e, what){
    switch(what){
      case "isPaid":
        console.log("IsPaid");
        break;
      case "isDateClose":
        console.log("isDateClose");
        break;
      case "isOutdatedAndNotPaid":
        console.log("isOutdatedAndNotPaid");
        break;
      case "isCanceled":
        console.log("isCanceled");
        break;
      default:
        console.log("default")
        break;
    }
  }
}
