import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DraftItemListService } from '../services/draft.list.item.service';
import { DraftListItem } from '../models/draft-item.model';
import { MonthsByName } from 'src/assets/resourses';

@Component({
  selector: 'abd-create-draft',
  templateUrl: './create-draft.component.html',
  styleUrls: ['./create-draft.component.css']
})
export class CreateDraftComponent implements OnInit {


  draft: DraftListItem
  monthNames: any
  draftPhoto: string
  now: any
  today: string
  constructor(private route: ActivatedRoute, private router: Router, private restApi: DraftItemListService) {
    this.monthNames = MonthsByName;
    this.now = new Date();
    let  day = ("0" + this.now.getDate()).slice(-2);
    let  month = ("0" + (this.now.getMonth() + 1)).slice(-2);
    this.today = this.now.getFullYear() + "-" + (month) + "-" + (day);

    this.draft = new DraftListItem(null, null, null, null, 0, 0, null, null, false, false, false, false, null, 0);
  }

  ngOnInit() {

  }

  onSubmit() {
    if(this.draft.photo && this.draft.photo !== undefined){
      let fileToUpload = <File>this.draft.photo[0];
      console.log(fileToUpload)
    }
    let formData = new FormData;

    let newDraft = {
        draftNumber: this.draft.draftNumber,
        supplier: this.draft.supplier,
        paymentDate: this.draft.paymentDate,
        value: this.draft.value,
        storedUpValue: this.draft.storedUpValue,
        photo: <File>this.draft.photo,
        notes: this.draft.notes,
        isPaid:  this.draft.isPaid,
        isCanceled: this.draft.isCanceled,
        isOutdatedAndNotPaid: this.draft.isOutdatedAndNotPaid
    }

    this.restApi.createDraft(newDraft).subscribe(( data: any) => {
      this.router.navigate(['/listagem-de-cheques'])
    });
  }

  onFileSelected(event){
    console.log(event);
  }
}
