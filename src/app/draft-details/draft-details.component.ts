import { Component, OnInit, Injectable, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DraftListItem } from '../models/draft-item.model';
import { DraftItemListService } from '../services/draft.list.item.service';
import { MonthsByName } from '../../assets/resourses';
import { EventEmiterService } from '../services/event.emiter.service';
import { getTodayFormatedDate, getDateDiffInDays } from '../Helpers/utils';

@Component({
  selector: 'abd-draft-details',
  templateUrl: './draft-details.component.html'

})
export class DraftDetailsComponent implements OnInit {


  draft: any
  isToUpdate: boolean
  isToUpdatedd: boolean
  monthNames: any
  id: any
  isUpdateAllowed: boolean
  path: string
  today: string

  descOrder = (a, b) => {
    if(a.key < b.key) return b.key;
  }

  constructor(private route: ActivatedRoute, private router: Router, private restApi: DraftItemListService, private eventEmiter: EventEmiterService) {
    this.isToUpdate = false
    this.monthNames = MonthsByName;
    this.isUpdateAllowed = true;
    this.today = getTodayFormatedDate();
  }

  ngOnInit() {
    this.draft = window.history.state
    this.id = this.draft.id === undefined ? this.route.snapshot.params.id : this.draft.id

    this.getDraft(this.id);
  }

  getDraft(id){

    return this.restApi.getDraft(id).subscribe(( data: any) => {
      debugger
      let isDateClose = getDateDiffInDays(this.today, data.paymentDate) >= 0 && getDateDiffInDays(this.today, data.paymentDate) <= 5;
      let isOutdatedAndNotPaid = getDateDiffInDays(this.today, data.paymentDate) < 0;

      this.draft = {
        DraftNumber: data.draftNumber,
        Supplier: data.supplier,
        PaymentDate: new Date,
        Value: data.value,
        StoredUpValue: data.storedUpValue,
        Photo: data.photo,
        Notes: data.notes,
        isPaid: data.isPaid,
        isCanceled: data.isCanceled,
        isDateClose: getDateDiffInDays(this.today, data.paymentDate) >= 0 && getDateDiffInDays(this.today, data.paymentDate) <= 5,
        isOutdatedAndNotPaid: getDateDiffInDays(this.today, data.paymentDate) < 0,
        id: data.id
      }

      this.path = (data.isPaid || data.isCanceled) ? "/lista-de-cheques-pagos-ou-anulados" : "/listagem-de-cheques"
      this.eventEmiter.sendMessage(`Cheque ${data.draftNumber}`)
    })
  }

  goToUpdateDraft() {
    this.router.navigate([['atualizar-cheque']])
  }
}
