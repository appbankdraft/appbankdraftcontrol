import { Component, OnInit, Input } from '@angular/core';
import { DraftDetailItem } from 'src/app/models/draft-detail-item.model';
import { FieldsName, MonthsByName, FormatAsText, FormatAsDate, FormatAsImage, FormatAsTextArea,  } from '../../../assets/resourses';
import { FormatAsCurrency, FormatAsRadioButton } from './../../../assets/resourses';

@Component({
  selector: 'abd-draft-details-item',
  templateUrl: './draft-details-item.component.html',
  styleUrls: ['./draft-details-item.component.css']
})
export class DraftDetailsItemComponent implements OnInit {


  @Input() data: DraftDetailItem
  aux: any
  translator: any
  months: any

  formatAsNormal: any
  formatAsDate: any
  formatAsCurrency: any
  formatAsImage: any
  formatAsTextArea: any
  formatAsRadioBtn: any
  isToUpdate: boolean
  isToUpdatedd: boolean


  constructor() {
    this.translator = FieldsName;
    this.months = MonthsByName;

    this.formatAsNormal = FormatAsText;
    this.formatAsDate = FormatAsDate;
    this.formatAsCurrency = FormatAsCurrency;
    this.formatAsImage = FormatAsImage;
    this.formatAsTextArea = FormatAsTextArea;
    this.formatAsRadioBtn = FormatAsRadioButton;
    this.isToUpdate = false;
    this.isToUpdatedd = false
  }

  ngOnInit() {
      if (this.data.Description === "PaymentDate") {
        this.data.Value = new Date(this.data.Value)
      }
  }

  public createImgPath = (serverPath: string) => {
    return `https://localhost:4200/${serverPath}`;
  }
}
