import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DraftItemListService } from '../services/draft.list.item.service';
import { SuppliersService } from './../services/supplier.service';

@Component({
  selector: 'abd-create-new-supplier',
  templateUrl: './create-new-supplier.component.html'
})
export class CreateNewSupplierComponent implements OnInit {

  isSaved: boolean
  _newSupplier: any
  suppliers: any = []

  constructor(private route: ActivatedRoute, private router: Router, private restApi: SuppliersService) {
    this.isSaved = false
    this._newSupplier = {id: null, Name: null}
  }

  ngOnInit() {
    this.getSuppliers();
  }

  getSuppliers(){
    return this.restApi.getSuppliers().subscribe(( data: {}) => {
      this.suppliers = data;
    })
  }

  onDelete(e, id){
    this.restApi.deleteSupplier(id).subscribe(( data: any) => {
      this.getSuppliers()
    });
  }

  onSubmit(){
    let supplier = {Name: this._newSupplier.Name}
    this.restApi.createSupplier(supplier).subscribe(( data: any) => {
      this.getSuppliers()
    });
  }

  sleep (time) {
    return new Promise((resolve) => setTimeout(resolve, time));
  }

}
