import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DraftListComponent } from './draft-list/draft-list.component';
import { DraftDetailsComponent } from './draft-details/draft-details.component';
import { CreateDraftComponent } from './create-draft/create-draft.component';
import { CreateNewSupplierComponent } from './create-new-supplier/create-new-supplier.component';
import { UpdateDraftComponent } from './update-draft/update-draft.component';
import { DraftPaidAndCanceledListComponent } from './draft-paid-and-canceled-list/draft-paid-and-canceled-list.component';

const routes: Routes = [
  { path: '', redirectTo:"/listagem-de-cheques", pathMatch: "full", data:{title:'Lista de Cheques'} },
  { path: 'listagem-de-cheques', component: DraftListComponent, data:{title:'Lista de Cheques'} },
  { path: 'lista-de-cheques-pagos-ou-anulados', component: DraftPaidAndCanceledListComponent, data:{title:'Cheques Pagos/Anulados'} },
  { path: 'cheque-detalhado/:id', component: DraftDetailsComponent, data:{title:'Detalhes do Cheque'} },
  { path: 'atualizar-cheque/:id', component: UpdateDraftComponent, data:{title:'Atualizar Cheque'} },
  { path: 'adicionar-novo-cheques', component: CreateDraftComponent, data:{title:'Novo Cheque'} },
  { path: 'criar-fornecedor', component: CreateNewSupplierComponent, data:{title:'Novo Fornecedor'} }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
